# image stimuli
# http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0020409
# http://tofu.psych.upenn.edu/~upennidb/


# parameters

import numpy as np
import datetime
from PIL import Image
import scipy.io
import scipy
import os
import random
import matplotlib.pyplot as plt
from scipy.io import savemat

para = {'framerate':0.02 ,  # (sec), Fixation every 10 ms
        'SaccadePeri':60 , # (sec), saccade times
        'FixationPeri':1 , # (sec), fixation between saccade
        'saccadesc':400 , # jitter withd for saccade
        'fixationsc':3 , # jitter withd for fixation
        'Ncheckers':49 , # number of gride/axis
        'expperiod':50 , # (min), total experiment time, image/min
        'shrinkratio':100 , # (%) resize the orignal image before crop
        'amplified':9 , # enlarge to 441 (433 pixel~electrode area), 1 pixel:3.27um
        'Nstimuli':60000,
        'imagepath':'/media/hydroassist/70B3597D6C53E436/Python/DataForNIPS/ImageData/',
        'datapath':'/media/hydroassist/70B3597D6C53E436/Python/DataForNIPS/'
        }

# image output folder
os.chdir(para['datapath'])
expday = datetime.datetime.now().strftime("%Y_%m_%d")
if not os.path.exists(expday+'_stiimg'):
    os.makedirs(expday+'_stiimg')
outimgpath = os.getcwd()+'/'+expday+'_stiimg'

# image np output folder
# if not os.path.exists(expday+'_npimg'):
#     os.makedirs(expday+'_npimg')
# outnpimgpath = os.getcwd()+'/'+expday+'_npimg'

expsize = para['Ncheckers']*para["amplified"]
imgarr = np.zeros([para['Nstimuli'],expsize,expsize],dtype="uint8")

# call image with random order
Nimgin = int(para['Nstimuli']/(para['FixationPeri']/para['framerate'])/(para['SaccadePeri']/para['FixationPeri']))
os.chdir(para['imagepath']) # image folder path
all_file = os.listdir(para['imagepath'])
random.shuffle(all_file)
if len(all_file)>Nimgin:
    all_file=all_file[0:Nimgin]

# create stimuli movie
#figure, h_r = axes;

count = 0;
for imagefile in all_file:
    imageraw = Image.open(imagefile).convert('L')
    img = np.asarray(imageraw,dtype ='uint8')
    # binimg = scipy.misc.imresize(img,para['shrinkratio'])
    binimg = img
    imgcentloc = np.double([int(binimg.shape[0]/2),int(binimg.shape[1]/2)])
    imginiloc = imgcentloc
    
    # create a image sequance with saccade and fixation
    # saccade every 1 sec
    for forj in range(int(para['SaccadePeri']/para['FixationPeri'])):
        movebias = (imgcentloc-imginiloc)/imgcentloc/2
        saccadelen = (np.random.rand(2)-0.5+movebias)*para['saccadesc']*2
        imginiloc = imginiloc + saccadelen.astype(int)         

        #fixation every 20 ms
        for fori in range(int(para['FixationPeri']/para['framerate'])):
            count = count+1
            fixationlen = (np.random.rand(2)-0.5)*para['fixationsc']*2
            imgloc = imginiloc + fixationlen.astype(int)
            halfchecker = para['Ncheckers']/2
            cropimg = binimg[int(imgloc[0]-halfchecker):int(imgloc[0]+halfchecker) ,int(imgloc[1]-halfchecker):int(imgloc[1]+halfchecker)]
            imgname = outimgpath+'/stimulus. '+'%05d' % count+'.png'
            im = Image.fromarray(cropimg)
            im.save(imgname)
            immat = scipy.misc.imresize(cropimg,para['amplified']*100)
            imgarr[fori] = immat
            # cv2.imwrite(imgname,cropimg)
            # npimgname = outnpimgpath+'/stimulus'+'%05d' % count+'.npy'
            # np.save(npimgname,cropimg)

# save crop index and image
os.chdir(para['datapath'])
np.save(expday+'_imgstitest.npy',para)
savemat(expday+'_imgstitest.mat',dict([('imgs', imgarr)]))

'finish processing'