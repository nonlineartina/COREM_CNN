% image stimuli
% http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0020409
% http://tofu.psych.upenn.edu/~upennidb/

close all
clear all

% parameters

para.framerate = 1/60; % (sec), Fixation every 10 ms
para.SaccadePeri = 60; % (sec), saccade times
para.FixationPeri = 1; % (sec), fixation between saccade
para.saccadesc = 100; % jitter width for saccade
para.fixationsc = 1; % jitter width for fixation
para.Ncheckers = 52; % number of gride/axis
para.expperiod = 50; % (min), total experiment time, image/min
para.shrinkratio = 1; % resize the orignal image before crop
para.amplified = 4; % 
imagepath = '/media/hydroassist/70B3597D6C53E436/Python/DataForNIPS/ImageData/';
datapath = '/media/hydroassist/70B3597D6C53E436/Python/DataForNIPS/';

% image output folder
cd(datapath)
formatOut = 'yyyy_mm_dd';
expday = datestr(now,formatOut);
if ~exist(expday, 'dir')
    mkdir([expday '_stiimg']);
end
outimgpath = [pwd '/' [expday '_stiimg']];

% call image with random order
cd(imagepath); % image folder path
all_file = dir('*.JPG');
if length(all_file)> para.expperiod
    para.NImg = randperm(length(all_file));
else
    para.NImg = [];
    for fori =1:ceil(para.expperiod/length(all_file))
        para.NImg = [para.NImg,randperm(length(all_file))];
    end
end

finalsize = para.Ncheckers * para.amplified;
bgimage = zeros(finalsize+1,finalsize+1);
% create stimuli movie
%figure, h_r = axes;
count = 0;
for fork = 1:para.expperiod
    imagefile = all_file(para.NImg(fork)).name;
    img = rgb2gray(imread(imagefile));
    binimg = imresize(img,para.shrinkratio);
    %figure; subplot(211); imagesc(img);subplot(212); imagesc(binimg); colormap(gray)
    imgcentloc = [fix(size(binimg,1)/2),fix(size(binimg,2)/2)];
%     imginiloc = imgcentloc;
    
    % create a image sequance with saccade and fixation
    % saccade every 1 sec
    for forj = 1: ceil(para.SaccadePeri/para.FixationPeri)
%         movebias = [imgcentloc-imginiloc]./imgcentloc/2-0.5;
%         saccadelen = round(((rand(1,2)-0.5)+movebias)*para.saccadesc*2);
%         imginiloc = imginiloc + saccadelen;       
        imginiloc = [round(imgcentloc(1)+(rand(1)-0.5)*size(binimg,1)),round(imgcentloc(2)+(rand(1)-0.5)*size(binimg,2))];
        dump = 0;
        while or(or(imginiloc(1)<para.Ncheckers/2+2,imginiloc(1)>size(binimg,1)/2+para.Ncheckers/2-2),or(imginiloc(2)<para.Ncheckers/2+2,imginiloc(2)>size(binimg,2)/2+para.Ncheckers/2-2))
            imginiloc = [round(imgcentloc(1)+(rand(1)-0.5)*size(binimg,1)),round(imgcentloc(2)+(rand(1)-0.5)*size(binimg,2))]
            dump = dump+1
        end
        %fixation every 60Hz
        for fori = 1:ceil(para.FixationPeri/para.framerate)
            count = count+1
            fixationlen = round((rand(1,2)-0.5)*para.fixationsc*2);
            imgloc = imginiloc + fixationlen;
            cropimg = mat2gray(imcrop(binimg,[fliplr(imgloc)-para.Ncheckers/2 ,para.Ncheckers-1,para.Ncheckers-1]));
            imgname = [outimgpath '/stimulus.' num2str(count.','%06d') '.mat'];
            LocSeq(count,:) = fliplr(imgloc)-para.Ncheckers/2;
            enlargeimg = imresize(cropimg,para.amplified,'nearest');
            bgimage(1:finalsize,1:finalsize)=enlargeimg;
            save(imgname,'bgimage')
        end
    end
end


'finish processing'